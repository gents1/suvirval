SURVIVAL/GENTS

Este projeto contém os arquivos necessários para configuração e inicialização do **Backend** e **Frontend** utilizando **Docker Container**. Os diretórios `back` e `front` possuem os arquivos `Dockerfile` e `docker-compose.yml`. Na raiz o arquivo `.gitlab-ci.yml` é responsável pelas configurações de **CI/CD** (Integração e Entrega Contínua), recurso utilizado para subir as versões em ambientes sem a necessidade de interferir manualmente para gerar os artefatos necessários para levantar os serviços em ambiente de produção.

## Tecnologias envolvidas
1. VueJS
2. Vuetify
3. Nuxt
4. Axios