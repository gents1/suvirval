# This file is a template, and might need editing before it works on your project.
FROM node:lts

LABEL version="1.0.0" description="Suvirval" maintainer="GENTS"

RUN mkdir /app

WORKDIR /app

COPY . /app

COPY ./package.json /app/package.json
COPY ./start.sh /app/start.sh

RUN cd /app; chmod 777 start.sh;
RUN cd /app; npm install; 
RUN cd /app; npm install; npm install -g serve;

EXPOSE 5000 

CMD ./front/start.sh
